import Component from '@ember/component';
import { A } from '@ember/array';



export default Component.extend({
  questroomTypes: A(['Эскейп-квест', 'Хоррор-квест']),
  questroomLevel: A(['Легкая', 'Средняя', 'Тяжелая']),
  questroomFear: A(['Детский','Подростковый','Взрослый'])
});
