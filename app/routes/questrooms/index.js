import Route from '@ember/routing/route';

export default Route.extend({
  model: function(){
    return [
      {
        title: "Зазеркалье"
      },
      {
        title: "Клаустрофобия"
      },
      {
        title: "Боязнь книг"
      }
    ]
  }
});
